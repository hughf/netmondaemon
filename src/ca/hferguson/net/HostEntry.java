package ca.hferguson.net;

import java.net.InetAddress;

public class HostEntry {
	private String hostname;
	private String ip;
	private boolean ipv6 = false;
	
	public HostEntry() {}
	
	public HostEntry(String ip, String name) {
		this.setIp(ip);
		this.setHostname(name);
		System.out.println(String.format("DEBUG Host: %s, ip %s", name, ip));
	}
	
	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public boolean isIpv6() {
		return ipv6;
	}

	public void setIpv6(boolean ipv6) {
		this.ipv6 = ipv6;
	}

	public byte[] getOctets() throws NumberFormatException {
		String delim = "\\.";
		if (ipv6)
			delim = "\\:";
		String[] octets = ip.split(delim);
		System.out.println(String.format("IP for %s has %d octets", this.ip, octets.length));
		byte[] ipBytes = new byte[octets.length];
		for (int i=0;i<octets.length;i++) {
			ipBytes[i] = (byte)Integer.parseInt(octets[i], ipv6?16:10);
		}
		System.out.println(String.format("byte array has %d digits", ipBytes.length));
		return ipBytes;
	}
	
	public InetAddress getInetObj() throws Exception {
		return InetAddress.getByAddress(getOctets());
	}
}
