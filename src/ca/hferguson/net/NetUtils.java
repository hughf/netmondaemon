package ca.hferguson.net;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Useful functions
 * @author Hugh Ferguson
 * 
 *
 */
public class NetUtils {
	
	public static boolean isIP6Address(InetAddress ia) {
		boolean retVal = false;
		byte[] addressBytes = ia.getAddress();
		if (addressBytes.length > 4)
			retVal = true;
		return retVal;
	}
	
	public static InetAddress GuessNetworkGateway(InetAddress ia) throws UnknownHostException {
		byte[] addressBytes = ia.getAddress();
		addressBytes[addressBytes.length-1] = 1;
		return InetAddress.getByAddress(addressBytes);
				
	}
}
