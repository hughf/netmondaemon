package ca.hferguson.net;


import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.simple.*;
import org.json.simple.parser.JSONParser;

public class RESTClient {
	private static final Logger log = LoggerFactory.getLogger(RESTClient.class);
	private String httpHost;	// Expect to be a FQ URL like http://hostname.com
	private CloseableHttpClient client;
	private JSONParser parser;
	
	public RESTClient(String httpHost) throws Exception {
		log.info("Creating REST client for host {}", httpHost);
		this.httpHost = httpHost;
		client = HttpClients.createDefault();
	}
	
	public JSONObject sendGet(String uri) {
		
		JSONObject jsonResp = null;
		HttpGet getReq = new HttpGet(getURL(uri));
		CloseableHttpResponse resp = null;
		try {
			resp = this.client.execute(getReq);
			jsonResp = processResponse(resp);
		} catch (Exception e) {
			log.error("Errow while sending GET request", e);
			jsonResp = exceptionToJSON(e);
		} finally {
			if (resp != null)
				try {
					resp.close();
				} catch (IOException e) {}
		}
		return jsonResp;
	}
	
	public JSONObject sendPost(String uri, JSONObject payload) {
		JSONObject jsonResp = null;
		HttpPost req = new HttpPost(getURL(uri));
		CloseableHttpResponse resp = null;
		try {
			StringEntity requestEntity = new StringEntity(payload.toJSONString(), ContentType.APPLICATION_JSON);
			req.setEntity(requestEntity);
			resp = this.client.execute(req);
			jsonResp = processResponse(resp);
		} catch (Exception e) {
			log.error("Error caught during POST request", e);
			jsonResp = exceptionToJSON(e);
		} finally {
			if (resp != null)
				try {
					resp.close();
				} catch (IOException e) {}
		}
		return jsonResp;
	}
	
	private JSONObject processResponse(CloseableHttpResponse resp) throws Exception {
		JSONObject jsonResp = null;
		if (resp.getStatusLine().getStatusCode() == 200) {
			// Get response body
			jsonResp = getResponseJSON(resp);
		} else {
			// come up with an error response
			jsonResp = getErrorJSON(resp);
		}
		return jsonResp;
	}
	
	private JSONObject getErrorJSON(CloseableHttpResponse resp) throws Exception {
		JSONObject jsonResp = new JSONObject();
		
		if (resp.getStatusLine().getStatusCode() >= 300) {
			jsonResp.put("status", String.valueOf(resp.getStatusLine().getStatusCode()));
			jsonResp.put("errorMsg", resp.getStatusLine().getReasonPhrase() );
		} else {
			jsonResp = getResponseJSON(resp);
		}
		
		
		return jsonResp;
	}
	
	private JSONObject exceptionToJSON(Exception e) {
		JSONObject jsonResp = new JSONObject();
		jsonResp.put("status", "500");
		jsonResp.put("errorMsg", e.getMessage());
		return jsonResp;
	}
	
	private JSONObject getResponseJSON(CloseableHttpResponse resp) throws Exception {
		if (parser == null)
			parser = new JSONParser();
		String body = getResponseBody(resp);
		log.debug("Response body {}", body);
		return (JSONObject)parser.parse(body);
	}
	private String getResponseBody(CloseableHttpResponse resp) throws Exception {
		HttpEntity entity = resp.getEntity();
		String retVal = EntityUtils.toString(entity, StandardCharsets.UTF_8);
		return retVal;
	}
	
	private String getURL(String uri) {
		
		StringBuffer sb = new StringBuffer(httpHost);
		if (!httpHost.endsWith("/"))
			sb.append("/");
		if (uri.startsWith("/"))
			uri = uri.substring(1);
		sb.append(uri);
		log.debug("GET URL is {}", sb.toString());
		return sb.toString();
		
	}
}
