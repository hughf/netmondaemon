package ca.hferguson.app;

import java.net.InetAddress;
/**
 * This class will get a host entry object from caller and 
 * ping it on interval set out in Propeties object passed in
 * After the cycle, it will send report to whoever is configured to
 * receive info.
 */
import java.util.*;
import ca.hferguson.net.*;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class MonitorThread implements Runnable {

	private static final Logger log = LoggerFactory.getLogger(MonitorThread.class);
	
	private Properties config;
	private HostEntry hostEntry;
	private String networkName;
	private int sleepInterval;
	private int pingTimeout;
	private boolean interrupted = false;
	private RESTClient client;
	
	public MonitorThread(Properties config, HostEntry hostInfo) {
		this.hostEntry = hostInfo;
		this.config = config;
		this.sleepInterval = getNumericParam("PING_INTERVAL", 10000);
		this.pingTimeout = getNumericParam("PING_TIMEOUT", 3000);
		this.networkName = getParameter("NETWORK_NAME");
		try {
		client = new RESTClient(getParameter("MONITOR_HOST_URL"));
		} catch (Exception e) {
			log.error("Exception caught while instantiating REST client", e);
			log.error("Shutting down thread");
			stop();
		}
	}
	
	public void stop() {
		log.info("STOP request received.");
		this.interrupted = true;
	}
	
	private int getNumericParam(String key, int defVal) {
		int retVal = defVal;
		try {
			String tmp = getParameter(key);
			if (tmp != null && tmp.length() > 0)
				retVal = Integer.parseInt(tmp);
		} catch (NumberFormatException e) {
			retVal = defVal;
		}
		return retVal;
	}
	
	private String getParameter(String key) {
		return this.config.getProperty(key);
	}
	@Override
	public void run() {
		InetAddress addrObj = null;
		while (!interrupted) {
			
			try {
				log.info("Pinging host {} with IP {}", hostEntry.getHostname(), hostEntry.getIp());
				if (addrObj == null)
					addrObj = this.hostEntry.getInetObj();
				long startTick = new Date().getTime();
				boolean isReachable = addrObj.isReachable(pingTimeout);
				// measure how long it took
				long latency = new Date().getTime() - startTick;
				if (!isReachable) {
					log.warn("No response from host {}", hostEntry.getHostname());
				} else {
					log.info("Response from host {} latency {} msec", hostEntry.getHostname(), latency);
				}
				sendUpdate(hostEntry.getHostname(), isReachable, latency);
				log.debug("Going to sleep");
				Thread.sleep(sleepInterval);
			} catch(Exception e) {
				e.printStackTrace();
				log.error("Got error while monitoring host {} : {}", hostEntry.getIp(), e.getMessage());
				interrupted = true;
			}
		}
		log.info("Monitoring thread for `{}` exiting", hostEntry.getHostname());
				
	}
	
	private void sendUpdate(String hostname, boolean isUp, long latency) throws Exception {
		JSONObject payload = new JSONObject();
		payload.put("eventtype", "pingtest");
		payload.put("network", this.networkName);
		payload.put("name", hostname);
		payload.put("up", isUp?"true":"false");
		payload.put("latency", String.valueOf(latency));
		
		JSONObject resp = client.sendPost("/api/event", payload);
		String status = (String)resp.get("status");
		String msg = (String)resp.get("msg");
		log.info("Response from server: {} {}", status, msg);
	}

}
