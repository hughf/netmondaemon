package ca.hferguson.app;

import ca.hferguson.net.*;
import java.io.*;
import java.net.*;
import java.util.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MonitorMaster {
	private Properties config;
	private static final int GATEWAY_TIMEOUT = 3000;
	private ArrayList<HostEntry> hostList;
	private RESTClient client;
	
	private static final Logger log = LoggerFactory.getLogger(MonitorMaster.class);
	
	public MonitorMaster() {
		log.error("Unsupported constructor");
	}
	
	public MonitorMaster(String[] args) {
		String defProperties = "./config.properties";
		String defHosts = "./hosts.json";
		if (args.length >0) {
			defProperties = args[0];
			if (args.length > 1) {
				defHosts = args[1];
			}
		}
		try {
			getConfig(defProperties);
			readHosts(defHosts);
			client = new RESTClient(getProperty("MONITOR_HOST_URL"));
			register();
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Exception caught. Exiting", e);
			System.exit(1);
		}
		
	}
	
	@SuppressWarnings("unchecked")
	public void register() throws Exception {
		JSONObject reg = new JSONObject();
		JSONArray hosts = new JSONArray();
		
		for (int i=0;i<hostList.size();i++) {
			HostEntry entry = hostList.get(i);
			String host = entry.getHostname();
			hosts.add(host);
		}
		reg.put("networkname", getProperty("NETWORK_NAME"));
		reg.put("hosts", hosts);
		JSONObject resp = client.sendPost("/api/register", reg);
		log.debug(resp.toJSONString());
	}
	
	public void monitor() {
		// Monitor may be extended from a base class, that's why we
		// don't just extend Thread.
		MonitorThread[] monObjs = new MonitorThread[getHostList().size()];
		Thread[] threads = new Thread[getHostList().size()];
		int i;
		log.info("Spinning up monitoring threads...");
		try {
			for (i = 0;i<getHostList().size();i++) {
				
				monObjs[i] = new MonitorThread(this.config, getHostList().get(i));
				threads[i] = new Thread(monObjs[i]);
				threads[i].start();
				
				// To stagger our calls, sleep for 5 seconds
				Thread.sleep(5000);
				
			}
			int shutdown = getIntProperty("SHUTDOWN_AFTER", 0);
			if (shutdown >0) {
				Thread.sleep(shutdown*1000);
				for (i=0;i<monObjs.length;i++ ) {
					monObjs[i].stop();
				}
			}
			// Otherwise, this app will continue running
			
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Exception caught in main application", e);
		}
	}
	private void getConfig(String defProperties) throws IOException {
		
		InputStream is = new FileInputStream(defProperties);
		this.config = new Properties();
		this.config.load(is);
	}
	
	private void readHosts(String defHosts) {
		JSONParser parser = new JSONParser();
		try {
			Object obj = parser.parse(new FileReader(defHosts));
			JSONObject root = (JSONObject)obj;
			JSONArray hosts = (JSONArray)root.get("hosts");
			if (!hosts.isEmpty()) {
				Iterator<JSONObject> iHosts = hosts.iterator();
				while (iHosts.hasNext()) {
					JSONObject hostEntry = iHosts.next();
					HostEntry entry = new HostEntry(hostEntry.get("ip").toString(), hostEntry.get("name").toString());
					getHostList().add(entry);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
		
	}
	/**
	 * Setup will attempt to find the best network interface to use
	 */
	public void networkDiscovery() {
		try {
			JSONObject initRest = client.sendGet("/api/ping");
			log.debug(initRest.toJSONString());
			Enumeration<NetworkInterface> nets = NetworkInterface.getNetworkInterfaces();
			while (nets.hasMoreElements()) {
				NetworkInterface ni = nets.nextElement();
				if (ni.isLoopback()) {
					log.info("{} is loopback, skipping", ni.getDisplayName());
					continue;
				}
				if (!ni.isUp()) {
					log.warn("Net adapter {} not up. Skipping", ni.getDisplayName());		
					continue;
				}
				log.info("Analysing NIC {} ({})", ni.getDisplayName(), ni.getName());
				Enumeration<InetAddress> ias = ni.getInetAddresses();
				
				while (ias.hasMoreElements()) {
					InetAddress ia = ias.nextElement();
					log.info("Host: {}, IP: {}", ia.getHostName(), ia.getHostAddress());
					if (ia.isAnyLocalAddress())
						log.info("Is local address");
					if (!NetUtils.isIP6Address(ia)) {
						log.info("This is a IPv4 address.  Attempting to use");
						InetAddress gw = NetUtils.GuessNetworkGateway(ia);
						log.info("Trying gateway {}", gw.getHostAddress());
						int pingTO = getTimeout();
						if (gw.isReachable(pingTO)) {
							log.info("Gatewy reachable");
						} else {
							log.warn("Gateway not reachable after {} msec", pingTO);
						}
						
					}
					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
		
	}
	
	private int getTimeout() {
		int timeout =  getIntProperty("PING_TIMEOUT", GATEWAY_TIMEOUT);
		
		return timeout;
	}
	
	private int getIntProperty(String key, int defValue) {
		int retVal = -1;
		try {
			retVal = Integer.parseInt(getProperty(key));
		} catch (Exception e) {
			retVal = defValue;
		}
		return retVal;
	}
	private String getProperty(String name) {
		String retVal = "";
		if (this.config != null) {
			retVal = (String)this.config.get(name);
		}
		return retVal;
	}
	
	private ArrayList<HostEntry> getHostList() {
		if (this.hostList == null)
			this.hostList = new ArrayList<HostEntry>();
		return this.hostList;
	}
	
	
	public static void main(String[] args) {
		MonitorMaster mm = new MonitorMaster(args);
		mm.monitor();
	}
}
