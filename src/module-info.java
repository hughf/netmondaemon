module ASNetworkMonitor {
	requires json.simple;
	requires log4j;
	requires slf4j.api;
	requires org.apache.httpcomponents.httpclient;
	requires org.apache.httpcomponents.httpcore;

}